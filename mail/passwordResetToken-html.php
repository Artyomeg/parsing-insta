<?php
 
use yii\helpers\Html;
 
$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_token]);

//$card = Card::find()->where(['card_id' => $user->card_id])->one();

?>
 
<div class="password-reset">
    <p>Здравствуйте, вы получили это письмо, потому что хотели восстановить пароль для входа в свой личный кабинет спортклуба Чемпион.</p>
    <p>Перейдите по ссылке ниже, чтобы восстановить пароль:</p>
    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
</div>