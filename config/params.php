<?php

return [
    'adminEmail' => 'info@tons.su',
    'loginWithEmail' => true,
    'user.passwordResetTokenExpire' => 3600,
    'systemFromEmail' => 'info@tons.su',
    'systemToEmail' => 'info@tons.su',
    'months' => [
        1 => 'января',
        2 => 'февраля',
        3 => 'марта',
        4 => 'апреля',
        5 => 'мая',
        6 => 'июня',
        7 => 'июля',
        8 => 'августа',
        9 => 'сентября',
        10 => 'октября',
        11 => 'ноября',
        12 => 'декабря',
    ],
    'instagram' => [
        'login' => 'xokamotima@p33.org',
        'password' => 'a123456',
    ]
];
