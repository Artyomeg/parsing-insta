<?php

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Логи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="log-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary' => "Показано <b>{begin} – {end}</b> из <b>{totalCount}</b>.",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'timestamp',
                'format' => 'raw',
                'value' => function ($model) {
                    return date("H:i d.m.y", strtotime($model->timestamp));
                },
            ],
            'text:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
