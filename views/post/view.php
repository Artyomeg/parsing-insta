<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Post */

$this->title = $model->post_shortcode;
$this->params['breadcrumbs'][] = ['label' => 'Посты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'id',
            'post_id',
//            'post_shortcode',
            [
                'attribute' => 'post_shortcode',
                'format' => 'raw',
                'value' => function ($model) {
                    return '<a href="https://www.instagram.com/p/' . $model->post_shortcode . '/" target="_blank">' . $model->post_shortcode . '</a>';
                },
            ],
            'owner_id',
//            'timestamp',
            'average_likes',
            'status',
            'after24',
            'liked_complete',
            'task_id',
            'location_id',
            'text',
        ],
    ]) ?>

</div>
