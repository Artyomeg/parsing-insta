<?php

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Посты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary' => "Показано <b>{begin} – {end}</b> из <b>{totalCount}</b>.",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'post_timestamp',
                'format' => 'raw',
                'value' => function ($model) {
                    return date("H:i d.m.y", strtotime($model->post_timestamp));
                },
            ],
            [
                'attribute' => 'found_timestamp',
                'format' => 'raw',
                'value' => function ($model) {
                    return date("H:i d.m.y", strtotime($model->found_timestamp));
                },
            ],
            [
                'attribute' => 'post_shortcode',
                'format' => 'raw',
                'value' => function ($model) {
                    return '<a href="https://www.instagram.com/p/' . $model->post_shortcode . '/" target="_blank">' . $model->post_shortcode . '</a>';
                },
            ],
            'status',
            [
                'attribute' => 'liked_complete',
                'format' => 'raw',
                'value' => function ($model) {
                    return
                        $model->liked_complete
                        ? $model->liked_complete . ': ' . $model->after24
                        : '<span class="not-set">(not set)</span>';
                },
            ],
            'owner_id',
            [
                'attribute' => 'text',
                'format' => 'raw',
                'value' => function ($model) {
                    // попробуем 100 символов
                    $str = (strlen($model->text) <= 60)
                        ? $model->text
                        : mb_substr($model->text, 0, 30, 'UTF-8'). '...';

                    return $str ;
                },
            ],
            // 'task_id',
            // 'location_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
