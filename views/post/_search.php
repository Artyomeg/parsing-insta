<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PostSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="post-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'post_id') ?>

    <?= $form->field($model, 'post_shortcode') ?>

    <?= $form->field($model, 'owner_id') ?>

    <?= $form->field($model, 'timestamp') ?>

    <?php // echo $form->field($model, 'average_likes') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'after24') ?>

    <?php // echo $form->field($model, 'liked_complete') ?>

    <?php // echo $form->field($model, 'task_id') ?>

    <?php // echo $form->field($model, 'location_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
