<?php
    use yii\helpers\Html;
    use yii\bootstrap\ActiveForm;

    $this->title = 'Регистрация в личном кабинете';
//    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
    <h1><?= Html::encode($this->title) ?></h1>
    <p>Если вы уже регистрировались нажмите <a href="/login">Вход</a></p>
    <div class="row">
        <div class="col-lg-5">
 
            <?php $form = ActiveForm::begin([
                    'fieldConfig' => [
                        'errorOptions' => ['encode' => false],
                    ],
                    'id' => 'form-signup'
                ]); ?>
                <?= $form->field($model, 'email')->textInput(['placeholder' => 'Ваш e-mail']) ?>
                <?= $form->field($model, 'password')->passwordInput(['placeholder' => 'Придумайте и запомните свой пароль']) ?>
                <div class="form-group">
                    <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                </div>
            <?php ActiveForm::end(); ?>
 
        </div>
    </div>
<!--    <div class="row">-->
<!--        <div class="col-lg-6">-->
<!-- -->
<!--            <p>Для чего вам личный кабинет?-->
<!--            <p>Для Вас откроется ряд возможностей:-->
<!--            <ul>-->
<!--                <li>Получать информацию о всех ваших посещениях клуба</li>-->
<!--                <li>Самостоятельно следить за остатками посещений по вашей карте</li>-->
<!--                <li>Узнать срок действия вашего абонемента</li>-->
<!--                <li>Написать письмо (отзыв, рекомендации, благодарности) руководству клуба</li>-->
<!--                <li>Подать он-лайн заявку на «заморозку» карты</li>-->
<!--            </ul>-->
<!-- -->
<!--        </div>-->
<!--    </div>-->
</div>