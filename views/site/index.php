<?php

use app\models\Task;
use app\models\Post;

$this->title = 'Главная';

$tasks = Task::find()->all();
$posts = Post::find()->all();
?>
<div class="site-index">

    <h1><?= $this->title ?></h1>

    <div class="body-content">

        <div class="row">
            <p>Всего задач: <?= count($tasks) ?></p>
            <p>Всего постов: <?= count($posts) ?></p>
            <p><a class="btn btn-primary" href="/system/parsebytag">Спарсить принудительно</a></p>
            <p><a class="btn btn-primary" href="/system/sendposts">Отправить принудительно</a></p>
            <p><a class="btn btn-primary" href="/system/after24">После 24ч принудительно</a></p>
        </div>
    </div>
</div>