<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SettingsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Настройки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="settings-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Управление аккаунтами', ['user-accounts/create'], ['class' => 'btn btn-link']) ?>
        <?= Html::a('Добавить настройку', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary' => "Показано <b>{begin} – {end}</b> из <b>{totalCount}</b>.",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            'comment',
            'value',
            'param',
//            'active',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
