<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\UsedInstagramAccount */

$this->title = 'Добавление аккаунта';
$this->params['breadcrumbs'][] = 'Настройки';
$this->params['breadcrumbs'][] = ['label' => 'Используемые аккаунты Instagram', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="used-instagram-account-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
