<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\UsedInstagramAccount */

$this->title = 'Обновление аккаунта: ' . $model->login;
$this->params['breadcrumbs'][] = 'Настройки';
$this->params['breadcrumbs'][] = ['label' => 'Используемые аккаунты Instagram', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->login, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Обновление';
?>
<div class="used-instagram-account-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
