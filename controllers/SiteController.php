<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;

use app\models\Log;
use app\models\LoginForm;
use app\models\Settings;
use app\models\SignupForm;
use app\models\PasswordResetRequestForm;
use app\models\ResetPasswordForm;

use InstagramScraper\Instagram;
use app\models\User;

class SiteController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Заявка отправлена. Проверьте свой e-mail.');
//                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Извините, мы не можем восстановить пароль.');
            }
        }

        return $this->render('passwordResetRequestForm', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password was saved.');
            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    /**
     * Личный кабинет - Главная.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * @return string
     */
    public function actionVk()
    {
        return $this->render('vk');
    }

    public function actionGetpost()
    {
        $post_id = "";
        $likes = "";
        if (file_get_contents('php://input')) {
            $dataset = trim(file_get_contents('php://input'));
        } else if (isset($_POST)) {
            $dataset = $_POST;
        }
        $dataset = htmlspecialchars ($dataset);
//        $dataset =

        if (isset($dataset['post_id'])) {
            $post_id = $dataset['post_id'];
        } else {
            echo "<h2>Произошла ошибка</h2>";
        }

        if (isset($dataset['likes'])) {
            $likes = $dataset['likes'];
        } else {
            echo "<h2>Произошла ошибка</h2>";
        }


        $settings = new Settings;
        $settings->param = json_encode($dataset);
        $settings->save();
    }

    public function actionSendpost()
    {
        $data = array(
            "data" => array(
                "post_id" => array( "BTwIBHWFCE8","BTwIBHWFCE8" ),
                "likes" => array( "5","10" )
            )
        );

        $data_string = json_encode($data);

        $ch = curl_init('http://ru.shop-followers.com/newRow.php');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );

        $result = curl_exec($ch);
        echo $result;
        curl_close($ch);
    }

    /**
     * Регистрация
     *
     * @return type
     */
    public function actionSignup()
    {
        $signupForm = new SignupForm();

        // если модель загрузилась из $_POST
        if ($signupForm->load(Yii::$app->request->post())) {
            // если юзер присвоился
            if ($user = $signupForm->signup()) {
                $b = $signupForm->sendSignupMail();

                // если мы залогинились
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $signupForm,
        ]);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest)
            return $this->goHome();

        $model = new LoginForm();

        if ($model->load(Yii::$app->request->post()) && $model->login())
            return $this->goBack();

        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function beforeAction($action)
    {
//        $this->enableCsrfValidation = false;

        $actionId = Yii::$app->controller->action->id;

        // если я гость
        if (Yii::$app->user->isGuest) {
            // и это не страница /login и не страница showcards
            if (
                ($actionId !== 'login')
                && ($actionId !== 'request-password-reset')
                && ($actionId !== 'reset-password')
                && ($actionId !== 'signup')
                && ($actionId !== 'showcards')
            ) {
                return $this->redirect('login');
            }
        }


        return parent::beforeAction($action);
    }
}
