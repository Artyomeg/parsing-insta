<?php

namespace app\controllers;

use app\commands\ExternalController;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;

class SystemController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Скрипт для тестирования
     */
    public function actionTest()
    {
        $controller = new ExternalController(Yii::$app->controller->id, Yii::$app);
        $controller->actionTest();
    }

    /**
     * Скрипт 1. Парсим по тегу.
     */
    public function actionParsebytag()
    {
        $controller = new ExternalController(Yii::$app->controller->id, Yii::$app);
        $controller->actionParsebytag();
    }

    /**
     * Скрипт 2. Отправляем исполнителю посты-задачи.
     */
    public function actionSendposts()
    {
        $controller = new ExternalController(Yii::$app->controller->id, Yii::$app);
        $controller->actionSendposts();
    }

    /**
     * Скрипт 3. Проверка после 24 часов.
     */
    public function actionAfter24()
    {
        $controller = new ExternalController(Yii::$app->controller->id, Yii::$app);
        $controller->actionAfter24();
    }

    public function beforeAction($action)
    {
        // если я гость
        if (Yii::$app->user->isGuest) {
            return $this->redirect('login');
        }

        return parent::beforeAction($action);
    }
}
