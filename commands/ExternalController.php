<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;

use Yii;
use app\models\Log;
use app\models\Post;
use app\models\Settings;
use app\models\Task;
use app\models\UsedInstagramAccount;
use InstagramScraper\Instagram;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class ExternalController extends Controller
{

    /**
     * Скрипт для тестирования
     */
    public function actionTest()
    {
        $instagram = UsedInstagramAccount::getLoggedInAccount();

        $media = $instagram->getMediaByCode('BPcyX0Cj-aF');

        $owner = $media->owner;

        $account = Instagram::getAccount($owner->username);

        var_dump($account);
        echo 'ok' . PHP_EOL;
    }

    /**
     * Скрипт 1. Парсим по тегу.
     */
    public function actionParsebytag()
    {
        $instagram = UsedInstagramAccount::getLoggedInAccount();

        // установим тег
        $tag = Settings::find()
            ->where(['param' => 'system_tag'])
            ->one()->value;

        // получим последние посты по тегу
        $pagination['hasNextPage'] = true;
        $postFound = false;

        // перебираем страницы пагинации инстаграма, пока не найдем пост, который уже парсили,
        // либо пока не дойдем до последней страницы пагинации
        while ($pagination['hasNextPage'] && !$postFound) {
            $pagination = $instagram->getPaginateMediasByTag($tag, $pagination['maxId']);

            // переберем посты на новой странице пагинации
            foreach ($pagination['medias'] as $media) {
                $test[] = $media->shortcode;

                // получим пост по
                $post = Post::find()->where(['post_id' => $media->id])->one();

                // если пост не найден, добавим его в бд
                if (!$post) {
                    $post = new Post;
                    $post->post_id = $media->id;
                    $post->post_shortcode = $media->shortcode;
                    $post->post_timestamp = date("Y-m-d H:i:s", $media->createdTime);
                    $post->text = Post::removeAllEmoji($media->caption);

                    // получим пост по шорткоду, чтобы получить верный locationId
                    $mediaByCode = $instagram->getMediaByCode($media->shortcode);
                    $post->location_id = $mediaByCode->locationId;
                    $owner_id = $mediaByCode->owner->id;
                    $post->owner_id = $owner_id;

                    $post->save();
                } else {
                    // установим флаг, что пост найден, и дальше смотреть не нужно
                    $postFound = true;
                    break;
                }
            }
        }

        // логируем парсинг
        Log::setNew('Первичный парсинг прошел.');

        // сообщим наЭкран / вКонсоль, что все ок
        echo 'ok' . PHP_EOL;
    }

    /**
     * Скрипт 2. Отправляем исполнителю посты-задачи.
     */
    public function actionSendposts()
    {
        // сделаем задержку на вполнение скрипта в 15 секунд
        sleep (15);

        // залогинимся
        $instagram = UsedInstagramAccount::getLoggedInAccount();

        // получим все посты
        $posts = Post::find()->where(['status' => Post::STATUS_NEW])->all();

        // получим все задачи
        $tasks = Task::find()
            ->where(['status' => Task::STATUS_ACTIVE])
            ->all();

        // заранее распарсим теги в задачах
        foreach ($tasks as $j => $task) {
            // распарсим теги
            $task_tags[$j] = explode(',', $task->tags);
        }

        // переберем все новые посты
        foreach ($posts as $i => $post) {

            // получим массив хештегов для данного поста
            preg_match_all('/#(\w+)/u', $post->text, $post_tags);
            $post_tags = $post_tags[1];

            // получим массив ссылок для данного поста
            preg_match_all('/@(\w+)/u', $post->text, $post_links);
            $post_links = $post_links[1];

            // переберем задачи применительно к этому посту
            foreach ($tasks as $j => $task) {
                // установим что до проверок пост проходит валидацию
                $validate = true;

                // если в задаче указана локация - проверим по локации
                if ($task->location_id) {
                    // если id локации задачи не соответствует id локации поста - переходим к следующей задаче
                    if ($task->location_id != $post->location_id) {
                        $validate = false;
                    }
                }

                // если в задаче указаны теги - проверим по тегам
                if ($validate && $task_tags[$j]) {
                    // переберем все теги для этой задачи
                    foreach ($task_tags[$j] as $task_tag) {
                        // если тег из задачи не нашелся в списке тегов поста - переходим к следующей задаче
                        if (!in_array(trim($task_tag), $post_tags)) {
                            $validate = false;
                        }
                    }
                }

                // если в задаче указана ссылка - проверим есть ли в тексте поста ссылка
                if ($validate && $task->link) {
                    // если тег из задачи не нашелся в списке тегов поста - переходим к следующей задаче
                    if (!in_array($task->link, $post_links)) {
                        $validate = false;
                    }
                }

                // если условия для данного поста выполнились, укажем для поста task_id равный номеру этой задачи
                if ($validate) {
                    $post->task_id = $task->id;
                    $post->save();
                    // остановим перебор задач
                    break;
                }
            }

            // Если условия ни для одной из задач не выполнились:
            // изменим статус поста на «not_match», сохраним пост и перейдем к другому посту.
            if (!$validate) {
                $post->status = Post::STATUS_NOT_MATCH;
                $post->save();
                continue;
            }

            // Делаем запрос к бд – получим все посты:
            // • отправленные (status == «sent»);
            // •	 старше текущего;
            // •	 в пределах cooldown дней;
            // •	 для задачи task_id.
            $cooldown_posts = Post::find()
                ->where(['status' => Post::STATUS_SENT])
                ->andWhere('post_timestamp > "' . date("Y-m-d H:i:s", strtotime($post->post_timestamp)) . '" - INTERVAL ' . (int)$task->cooldown . ' DAY')
                ->andWhere('post_timestamp < "' . date("Y-m-d H:i:s", strtotime($post->post_timestamp)) . '"')
                ->andWhere(['owner_id' => $post->owner_id])
                ->andWhere(['task_id' => $post->task_id])
                ->all();

            // если нашлись посты, которые в рамках времени кулдауна - значит текущий будет в кулдауне
            // и перейдем к следующему посту
            if (!empty($cooldown_posts)) {
                $post->status = Post::STATUS_COOLDOWN;
                $post->save();
                continue;
            }

            // получим запись
            $media = $instagram->getMediaByCode($post->post_shortcode);

            // если media не найден - вернем false и пометим пост как hidden_after_first_parse
            if (!$media) {
                $post->status = Post::STATUS_HIDDEN_AFTER_FIRST_PARSE;
                $post->save();
                continue;
            }

            // получим username хозяина записи
            $owner_username = $media->owner->username;

            // получим последние 50 записей владельца текущего поста (работает без авторизации)
            $owner_medias = Instagram::getMedias($owner_username, 50);

            $owner_medias_structure = [];
            $owner_medias_sum = 0;
            $counter_enable = false;

            // переберем посты владельца текущего поста,
            // пока не наткнемся на наш пост
            foreach ($owner_medias as $owner_media) {
                // 1. Если отчсчет идет - учтем пост
                if ($counter_enable) {
                    $owner_medias_structure[] = $owner_media->likesCount;
                    $owner_medias_sum += $owner_media->likesCount;
                }

                // 2. Если подсчет еще не начат
                if (!$counter_enable) {
                    // 2. Если наткнулись на наш текущий пост - начнем подсчет
                    if ($owner_media->shortcode == $post->post_shortcode) {
                        $counter_enable = true;
                    }
                }

                // 3. Проверим - не пора ли закончить подсчет
                if (count($owner_medias_structure) == 3) {
                    break;
                }
            }


            // подсчитаем среднее число лайков
            if (count($owner_medias_structure) == 0) {
                $average_likes = 0;
            } else {
                // подсчитаем среднее число лайков, исходя из количества посчитанных постов
                $average_likes = round($owner_medias_sum / count($owner_medias_structure));
            }

            // зафиксируем первичное среднее значение лайков на аккаунте
            $post->average_likes = $average_likes;
            $post->save();

            $to_send['post_id'][] = $post->post_shortcode;
            $to_send['likes'][] = $post->getSecondaryAverageLikes();
        }

        // если массив не собрался, ничего не отправляем
        if (!$to_send) {
            // логируем отсутствие отправки данных
            Log::setNew('Отправка данных не нужна - нечего отправлять.');

            exit();
        }

        // закатаем данные для отправки в json
        $dataString = json_encode([
            "data" => $to_send
        ]);

        // получим url для отправки
        $url_to_send = Settings::find()
            ->where(['param' => 'primary_url_to_send'])
            ->one()->value;

        // если это продакшен - отправим post-запрос
        if ($_SERVER['HTTP_HOST'] != "insta.os") {
            // сформируем curl-запрос
            $ch = curl_init($url_to_send);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($dataString)]
            );

            // отправил curl-запрос
            $result = curl_exec($ch);
            // разгрузим curl
            curl_close($ch);
//        echo $result;
        }

        // переберем посты чтобы зафиксировать их отправку
        foreach ($posts as $i => $post) {
            // если пост был отправлен - зафиксируем его новый статус
            if (in_array($post->post_shortcode, $to_send['post_id'])) {
                $post->status = Post::STATUS_SENT;
                $post->save();
            }
        }

        // логируем отсутствие отправки данных
        Log::setNew('Отправка данных прошла. Записей отправлено: ' . count(count($to_send['post_id'])));

        // сообщим наЭкран / вКонсоль, что все ок
        echo 'ok' . PHP_EOL;
    }

    /**
     * Скрипт 3. Проверка после 24 часов.
     */
    public function actionAfter24()
    {
        // получаем отправленные записи в диапазоне 24-26 часов раньше текущего времени
        $postsQuery = Post::find()
            ->where(['status' => Post::STATUS_SENT])
            ->andWhere('found_timestamp < NOW() - INTERVAL 10 MINUTE')
            ->andWhere(['IS', 'liked_complete', null]);
//            ->andWhere('found_timestamp < NOW()')
//            ->all();

        echo $postsSql = $postsQuery->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql . '<br/>';

        $posts = $postsQuery->all();
        echo 'found ' . count($posts) . '<br/>';

        // залогинимся
        $instagram = UsedInstagramAccount::getLoggedInAccount();

        foreach ($posts as $post) {
            $media = $instagram->getMediaByCode($post->post_shortcode);

            // если пост не нашелся - установим соответствующий статус
            if (!$media) {
                $post->status = Post::STATUS_DELETE_AFTER_24;
                $post->save();
                continue;
            }

            // фиксируем в модели поста данные о количестве лайков
            $post->after24 = $media->likesCount;

            // посчитаем сколько раз должны были пролайкать
            $secondaryAverageLikes = $post->getSecondaryAverageLikes();

            if ($post->after24 >= $secondaryAverageLikes) {
                $post->liked_complete = 'yes';
            } else {
                $post->liked_complete = 'no';
            }

            // сохраним изменения в модели записи
            $post->save();
        }

        // логируем отсутствие отправки данных
        if (count($posts)) {
            Log::setNew('Проверка после 24 часов прошла. Постов обработано: ' . count($posts));
        }
        else {
            Log::setNew('Проверка после 24 часов прошла. Нет постов для обработки.');
        }
        // сообщим наЭкран / вКонсоль, что все ок
        echo 'ok' . PHP_EOL;
    }

}
