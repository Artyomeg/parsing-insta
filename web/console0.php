<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

//exit();

// comment out the following two lines when deployed to production
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

require(__DIR__ . '/../vendor/autoload.php');
require(__DIR__ . '/../vendor/yiisoft/yii2/Yii.php');

$config = require(__DIR__ . '/../config/console.php');

// обработка консольных комманд
if (isset($_GET['r']) and !empty($_GET['r'])) {
    // ($_SERVER['DOCUMENT_ROOT'])
    if ($_SERVER['HTTP_HOST'] == "insta.os") {
        // локальный сайт
        $argvPath = 'C:/OpenServer/domains/insta.os/www';
    } elseif ($_SERVER['HTTP_HOST'] == "tons.su") {
        // продакшн
        $argvPath = '';
    }

    $_SERVER['argv'] = [
        $argvPath,
        $_GET['r']
    ];

    $_SERVER['argc'] = 2;
}

$application = new yii\console\Application($config);
$exitCode = $application->run();
exit($exitCode);
