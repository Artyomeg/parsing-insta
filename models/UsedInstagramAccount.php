<?php

namespace app\models;

use Yii;
use app\models\Settings;
use InstagramScraper\Instagram;

/**
 * This is the model class for table "used_instagram_account".
 *
 * @property string $id
 * @property string $login
 * @property string $password
 */
class UsedInstagramAccount extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'used_instagram_account';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['login', 'password'], 'string', 'max' => 255],
            [['login'], 'unique', 'message' => 'Такой e-mail уже используется.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'login' => 'Логин (e-mail)',
            'password' => 'Пароль',
        ];
    }


    /**
     * Находим в настройках id аккаунта и логинимся по нему
     *
     * @return \app\models\UsedInstagramAccount
     */
    public static function getLoggedInAccount()
    {
        // получим id текущего аккаунта
        $find_account = Settings::find()
            ->where(['param' => 'used_account'])
            ->one();

        // если аккаунт не нашелся - получаем другой аккаунт
        if (!$find_account) {
            // логируем ошибку ненахождения аккаунта в настройках
            Log::setNewError(
                'Аккаунт не найден в настройках.',
                __CLASS__,
                __LINE__
            );
        }

        $account_id = $find_account->value;

        // получаем текущий аккаунт на основе id
        $account = self::find()
            ->where(['id' => $account_id])
            ->one();


        // если аккаунт получен - пробуем логиниться
        if ($account) {
            // логинимся
            $instagram = Instagram::withCredentials($account->login, $account->password);

            // пробуем логиниться, и прокатило - фиксируем и возвращаем Instagram
            if (!$instagram->login()) {
                $instagram = self::getAnotherAccount($account->id);
            }
        } else {
            // если аккаунт не найден - сразу ищем другой рабочий аккаунт
            $instagram = self::getAnotherAccount();
        }

        return $instagram;
    }

    /**
     * Получим аккаунт, через который можно работать
     *
     * @param null $account_id
     * @return Instagram
     */
    public static function getAnotherAccount($account_id = null)
    {
        // получим все аккаунты
        $all_accounts = self::find()
            ->all();

        // преобразуем массив в тот вид, в котором нам будет удобно с ним работать
        foreach ($all_accounts as $account) {
            $structured_accounts[$account->id] = [
                'bad_trying' => 0,
                'account' => $account
            ];
        }

        // если по какому-то аккаунту уже облажались - зафиксируем это
        if ($account_id) {
            $structured_accounts[$account_id]['bad_trying'] = 1;
        }

        // переберем все аккаунты, чтобы найти не забаненый
        foreach ($structured_accounts as $structure) {
            // если для текущего аккаунта уже зафиксирована плохая попытка - пропустим его
            if ($structure['bad_trying'] == 1) {
                continue;
            }

            // закешируем аккаунт, чтобы было удобно с ним работать
            $account = $structure['account'];

            // зафиксруем данные логина
            $instagram = Instagram::withCredentials($account->login, $account->password);

            // пробуем логиниться, и прокатило - фиксируем и возвращаем Instagram
            if ($instagram->login()) {
                // зафиксируем в настройках новый текущий аккаунт
                $settings_account = Settings::find()
                    ->where(['param' => 'used_account'])
                    ->one();

                $settings_account->value = $account->id;
                $settings_account->save();

                // запишем в логи смену аккаунта
                Log::setNew(
                    'Смена аккаунта на #' . $account->id . '.',
                    __CLASS__,
                    __LINE__
                );

                // вернем объект Instagram
                return $instagram;
            } else {
                $structure['bad_trying'] = 1;
                // если выбросили ошибку
                continue;
            }
        }

        // выдаем ошибку в логи, если аккаунт не нашелся
        Log::setNewError(
            'Аккаунт не нашелся даже методом перебора всех аккаунтов.',
            __CLASS__,
            __LINE__
        );
    }

}
