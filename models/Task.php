<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "task".
 *
 * @property string $id
 * @property string $title
 * @property string $tags
 * @property string $location_id
 * @property string $link
 * @property string $cooldown
 * @property string $distributor
 * @property string $create_timestamp
 * @property string $status
 */
class Task extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 'active';
    const STATUS_INACTIVE = 'inactive';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cooldown'], 'number'],
            [['create_timestamp'], 'safe'],
            [['title', 'tags', 'location_id', 'link', 'distributor', 'status'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'tags' => 'Теги',
            'location_id' => 'ID локации',
            'link' => 'Ссылка (@)',
            'cooldown' => 'Кулдаун, дней',
            'distributor' => 'Дистрибьютор (кто привел клиента)',
            'create_timestamp' => 'Дата создания',
            'status' => 'Состояние',
        ];
    }
}
