<?php

namespace app\models;

use Yii;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "log".
 *
 * @property string $id
 * @property string $timestamp
 * @property string $text
 */
class Log extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['timestamp'], 'safe'],
            [['text'], 'required'],
            [['text'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'timestamp' => 'Дата и время',
            'text' => 'Текст',
        ];
    }

    /**
     * Удобная функция добавления нового лога состояния.
     *
     * @param $text
     */
    public static function setNew($text) {
        // логируем парсинг
        $log = new self;
        $log->text = $text;
        $log->save();
    }

    /**
     * Удобная функция добавления нового лога ошибки.
     *
     * @param $text
     */
    public static function setNewError($text, $class, $line) {
        // логируем парсинг
        $log = new self;
        $log->text = 'ERROR. Класс ' . $class . '/ ::' . $line . '. ' . $text;
        $log->save();

        throw new NotFoundHttpException($text);
    }
}
