<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Post;

/**
 * PostSearch represents the model behind the search form about `app\models\Post`.
 */
class PostSearch extends Post
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'owner_id', 'average_likes', 'after24', 'task_id'], 'integer'],
            [['post_id', 'post_shortcode', 'post_timestamp', 'status', 'liked_complete', 'location_id', 'found_timestamp'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Post::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['post_timestamp' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'owner_id' => $this->owner_id,
            'post_timestamp' => $this->post_timestamp,
            'average_likes' => $this->average_likes,
            'after24' => $this->after24,
            'task_id' => $this->task_id,
            'found_timestamp' => $this->found_timestamp,
        ]);

        $query->andFilterWhere(['like', 'post_id', $this->post_id])
            ->andFilterWhere(['like', 'post_shortcode', $this->post_shortcode])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'liked_complete', $this->liked_complete])
            ->andFilterWhere(['like', 'location_id', $this->location_id]);

        return $dataProvider;
    }
}
