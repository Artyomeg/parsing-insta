<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "post".
 *
 * @property string $id
 * @property string $post_id
 * @property string $post_shortcode
 * @property integer $owner_id
 * @property string $post_timestamp
 * @property integer $average_likes
 * @property string $status
 * @property integer $after24
 * @property string $liked_complete
 * @property integer $task_id
 * @property string $location_id
 * @property string $found_timestamp
 */
class Post extends \yii\db\ActiveRecord
{
    const STATUS_NEW = 'new';
    const STATUS_NOT_MATCH = 'not_match';
    const STATUS_SENT = 'sent';
    const STATUS_COOLDOWN = 'cooldown';
    const STATUS_HIDDEN_AFTER_FIRST_PARSE = 'hidden';
    const STATUS_DELETE_AFTER_24 = 'delete_after_24';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['owner_id', 'average_likes', 'after24', 'task_id'], 'integer'],
            [['post_timestamp', 'found_timestamp'], 'safe'],
            [['post_id', 'post_shortcode', 'status', 'location_id'], 'string', 'max' => 255],
            [['liked_complete'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'post_id' => 'ID поста',
            'post_shortcode' => 'Шорткод',
            'owner_id' => 'ID хозяина',
            'post_timestamp' => 'Время создания поста',
            'average_likes' => 'Среднее число лайков',
            'status' => 'Статус',
            'after24' => 'После 24ч',
            'liked_complete' => 'Пролайкано',
            'task_id' => 'ID задачи',
            'location_id' => 'ID места',
            'found_timestamp' => 'Время парсинга поста',
        );
    }

    public function getSecondaryAverageLikes()
    {
        if ($this->average_likes < 50) {
            $average_likes_secondary = 100;
        } elseIf ($this->average_likes > 500) {
            $average_likes_secondary = 1000;
        } else {
            $average_likes_secondary = $this->average_likes * 2;
        }

        return $average_likes_secondary;
    }

    public static function removeAllEmoji($text) {
        $text = self::removeEmoji1($text);
        $text = self::removeEmoji2($text);
        $text = self::removeEmoji3($text);
        $text = self::removeEmoji4($text);

        return $text;
    }

    public static function removeEmoji1($text)
    {
        $text1 = preg_replace('/[[:^print:]]/', '', $text);
        return preg_replace('/([0-9|#][\x{20E3}])|[\x{00ae}|\x{00a9}|\x{203C}|\x{2047}|\x{2048}|\x{2049}|\x{3030}|\x{303D}|\x{2139}|\x{2122}|\x{3297}|\x{3299}][\x{FE00}-\x{FEFF}]?|[\x{2190}-\x{21FF}][\x{FE00}-\x{FEFF}]?|[\x{2300}-\x{23FF}][\x{FE00}-\x{FEFF}]?|[\x{2460}-\x{24FF}][\x{FE00}-\x{FEFF}]?|[\x{25A0}-\x{25FF}][\x{FE00}-\x{FEFF}]?|[\x{2600}-\x{27BF}][\x{FE00}-\x{FEFF}]?|[\x{2900}-\x{297F}][\x{FE00}-\x{FEFF}]?|[\x{2B00}-\x{2BF0}][\x{FE00}-\x{FEFF}]?|[\x{1F000}-\x{1F6FF}][\x{FE00}-\x{FEFF}]?/u', ' ', $text);
    }

    public static function removeEmoji2($text) {

        $clean_text = "";

        // Match Emoticons
        $regexEmoticons = '/[\x{1F600}-\x{1F64F}]/u';
        $clean_text = preg_replace($regexEmoticons, ' ', $text);

        // Match Miscellaneous Symbols and Pictographs
        $regexSymbols = '/[\x{1F300}-\x{1F5FF}]/u';
        $clean_text = preg_replace($regexSymbols, ' ', $clean_text);

        // Match Transport And Map Symbols
        $regexTransport = '/[\x{1F680}-\x{1F6FF}]/u';
        $clean_text = preg_replace($regexTransport, ' ', $clean_text);

        // Match Miscellaneous Symbols
        $regexMisc = '/[\x{2600}-\x{26FF}]/u';
        $clean_text = preg_replace($regexMisc, ' ', $clean_text);

        // Match Dingbats
        $regexDingbats = '/[\x{2700}-\x{27BF}]/u';
        $clean_text = preg_replace($regexDingbats, ' ', $clean_text);

        return $clean_text;
    }

    public static function removeEmoji3($text) {
        $clean_text = "";

        // Match Emoticons
        $regexEmoticons = '/[\x{1F600}-\x{1F64F}]/u';
        $clean_text = preg_replace($regexEmoticons, '', $text);

        // Match Miscellaneous Symbols and Pictographs
        $regexSymbols = '/[\x{1F300}-\x{1F5FF}]/u';
        $clean_text = preg_replace($regexSymbols, '', $clean_text);

        // Match Transport And Map Symbols
        $regexTransport = '/[\x{1F680}-\x{1F6FF}]/u';
        $clean_text = preg_replace($regexTransport, '', $clean_text);

        // Match Miscellaneous Symbols
        $regexMisc = '/[\x{2600}-\x{26FF}]/u';
        $clean_text = preg_replace($regexMisc, '', $clean_text);

        // Match Dingbats
        $regexDingbats = '/[\x{2700}-\x{27BF}]/u';
        $clean_text = preg_replace($regexDingbats, '', $clean_text);

        // Match Flags
        $regexDingbats = '/[\x{1F1E6}-\x{1F1FF}]/u';
        $clean_text = preg_replace($regexDingbats, '', $clean_text);

        // Others
        $regexDingbats = '/[\x{1F910}-\x{1F95E}]/u';
        $clean_text = preg_replace($regexDingbats, '', $clean_text);

        $regexDingbats = '/[\x{1F980}-\x{1F991}]/u';
        $clean_text = preg_replace($regexDingbats, '', $clean_text);

        $regexDingbats = '/[\x{1F9C0}]/u';
        $clean_text = preg_replace($regexDingbats, '', $clean_text);

        $regexDingbats = '/[\x{1F9F9}]/u';
        $clean_text = preg_replace($regexDingbats, '', $clean_text);

        return $clean_text;
    }

    public static function removeEmoji4($string) {
        return preg_replace('/&#x(e[0-9a-f][0-9a-f][0-9a-f]|f[0-8][0-9a-f][0-9a-f])/i', '', $string);
    }

}
