<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\User;

/**
 * Signup form
 */
class SignupForm extends Model {

    public $email;
    public $password;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [['email', 'trim'],
            ['email', 'required', 'message' => '{attribute} не может быть пустым.'],
            ['email', 'email', 'message' => '{attribute} введен некорректно.'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\app\models\User', 'message' => 'Пользователь с этим e-mail уже зарегистрирован. <a href="/request-password-reset">Восстановить&nbsp;пароль</a>.'],
            ['password', 'required', 'message' => '{attribute} не может быть пустым.'],
            ['password', 'string', 'min' => 6, 'tooShort' => "{attribute} не может быть меньше 6 символов."],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'password' => 'Пароль',
            'email' => 'E-mail',
            'fio' => 'ФИО',
            'last_operation' => 'Последняя операция ',
        ];
    }
    
    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup() {
        if (!$this->validate())
            return null;

        $user = new User();
        $user->email = $this->email;
        $user->role = User::ROLE_NEW_USER;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        
        return $user->save() ? $user : null;
    }

    public function sendSignupMail() {
        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'signup-html', 'text' => 'passwordResetToken-text'],
                ['signupForm' => $this]
            )
            ->setFrom([Yii::$app->params['systemFromEmail'] => Yii::$app->name])
            ->setTo($this->email)
            ->setSubject(Yii::$app->name . '. Регистрация в личном кабинете.')
            ->send();
    }
}